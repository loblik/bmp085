#include <stdio.h>
#include "bmp085.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "usage: bmp085-get [I2C DEV]\n");
        return 1;
    }
    int ret;
    struct bmp085 bmp;
    ret = bmp085_init(&bmp, argv[1]);
    if (ret != BMP085_OK)
        return 2;

    bmp085_set_mode(&bmp, BMP085_ULTRA_HIGH_RES);

    int temp, press;
    float altitude, sea;

    ret = bmp085_read_press(&bmp, &press, &temp);
    if (ret != BMP085_OK)
        return 2;

    altitude = bmp085_press_to_altitude(102300, press);
    sea = bmp085_press_to_sea(press, 295.0);
    printf("sea: %.2f alt: %.2f temp: %.2f\n", sea / 100.0, altitude, temp / 10.0);

    return 0;
}
