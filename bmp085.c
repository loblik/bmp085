#define _BSD_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <math.h>
#include "bmp085.h"

#define BMP085_COEFS_ADDR       0xaa
#define BMP085_COEFS_NUM          11

#define BMP085_REG_CMD          0xf4
#define BMP085_CMD_TEMP         0x2e
#define BMP085_CMD_PRESS_BASE   0x34
#define BMP085_RESULT_REG       0xf6
#define BMP085_RESULT_REG_2     0xf8

#define BMP085_TEMP_CONV_TIME   4500

static int bmp085_read(struct bmp085 *bmp, unsigned char *buffer, int n);
static int bmp085_write(struct bmp085 *bmp, const unsigned char *buffer, int n);
static int bmp085_read_coefs(struct bmp085 *bmp);
static int bmp085_read_ut(struct bmp085 *bmp, uint16_t *ut);
static int bmp085_read_up(struct bmp085 *bmp, uint32_t *up);
static long bmp085_calculate_b5(struct bmp085 *bmp, uint16_t ut);

static useconds_t bmp085_conv_time[] = { 4500, 7500, 13500, 25500 };

void bmp085_set_mode(struct bmp085 *bmp, enum bmp085_mode mode) {
    bmp->mode = mode;
}

int bmp085_write(struct bmp085 *bmp, const unsigned char *buffer, int n) {
    int ret = write(bmp->fd, buffer, 2);
    if (ret < 0) {
        perror("i2c write");
        return BMP085_BUS_ERROR;
    }
    return BMP085_OK;
}

int bmp085_read(struct bmp085 *bmp, unsigned char *buffer, int n) {
    int ret = read(bmp->fd, buffer, n);
    if (ret < 0) {
        perror("i2c read");
        return BMP085_BUS_ERROR;
    }
    return BMP085_OK;
}

static int bmp085_read_coefs(struct bmp085 *bmp) {
    unsigned char buffer[2];
    int16_t *coef = (int16_t*)&bmp->coefs;
    int i;
    for (i = 0; i < BMP085_COEFS_NUM; i++) {
        unsigned char cmd_buffer[] = { BMP085_COEFS_ADDR + 2*i };
        int ret = bmp085_write(bmp, cmd_buffer, 1);
        if (ret != BMP085_OK)
            return ret;

        ret = bmp085_read(bmp, buffer, 2);
        if (ret != BMP085_OK)
            return ret;

        *coef++ = ((int16_t)buffer[0] << 8) | buffer[1];
    }
    return BMP085_OK;
}

static int bmp085_read_ut(struct bmp085 *bmp, uint16_t *ut) {
    unsigned char buffer[2];
    unsigned char cmd_buffer[] = { BMP085_REG_CMD, BMP085_CMD_TEMP };
    int ret = bmp085_write(bmp, cmd_buffer, sizeof(cmd_buffer)/sizeof(cmd_buffer[0]));
    if (ret != BMP085_OK)
        return ret;

    usleep(BMP085_TEMP_CONV_TIME);

    ret = bmp085_write(bmp, (unsigned char[]){ BMP085_RESULT_REG }, 1);
    if (ret != BMP085_OK)
        return ret;

    ret = bmp085_read(bmp, buffer, 2);
    if (ret != BMP085_OK)
        return ret;

    *ut = (uint16_t)(buffer[0] << 8) | buffer[1];
    return BMP085_OK;
}

static int bmp085_read_up(struct bmp085 *bmp, uint32_t *up) {
    unsigned char buffer[] = { 0, 0, 0 };
    unsigned char cmd_buffer[] = { BMP085_REG_CMD,
        BMP085_CMD_PRESS_BASE + (bmp->mode << 6) };
    int ret = bmp085_write(bmp, cmd_buffer, sizeof(cmd_buffer)/sizeof(cmd_buffer[0]));
    if (ret != BMP085_OK)
        return ret;

    usleep(bmp085_conv_time[bmp->mode]);

    ret = bmp085_write(bmp, (unsigned char[]){ BMP085_RESULT_REG }, 1);
    if (ret != BMP085_OK)
        return ret;

    ret = bmp085_read(bmp, buffer, 2);
    if (ret != BMP085_OK)
        return ret;

    ret = bmp085_write(bmp, (unsigned char[]){ BMP085_RESULT_REG_2 }, 1);
    if (ret != BMP085_OK)
        return ret;

    ret = bmp085_read(bmp, buffer + 2, 1);
    if (ret != BMP085_OK)
        return ret;

    *up = (buffer[0] << 16 | buffer[1] << 8 | buffer[2]) >> (8 - bmp->mode);
    return BMP085_OK;
}

static long bmp085_calculate_b5(struct bmp085 *bmp, uint16_t ut) {
    long x1 = (((long)ut - bmp->coefs.ac6) * bmp->coefs.ac5) >> 15;
    long x2 = (bmp->coefs.mc << 11) / (x1 + bmp->coefs.md);
    return x1 + x2;
}

int bmp085_read_temp(struct bmp085 *bmp, int *t) {
    uint16_t ut;
    long b5;
    int ret = bmp085_read_ut(bmp, &ut);
    if (ret != BMP085_OK)
        return ret;

    b5 = bmp085_calculate_b5(bmp, ut);
    *t = ((b5 + 8) >> 4);
    return BMP085_OK;
}

int bmp085_read_press(struct bmp085 *bmp, int *press, int *temp) {
    long x1, x2, x3, b3, b6, p;
    unsigned long b4, b7;
    uint16_t ut;
    uint32_t up;
    int ret;
    ret = bmp085_read_ut(bmp, &ut);
    if (ret != BMP085_OK)
        return ret;

    ret = bmp085_read_up(bmp, &up);
    if (ret != BMP085_OK)
        return ret;

    long b5 = bmp085_calculate_b5(bmp, ut);
    *temp = ((b5 + 8) >> 4);

    b6 = b5 - 4000;
    /* calculate B3 */
    x1 = (bmp->coefs.b2 * (b6 * b6 >> 12)) >> 11;
    x2 = (bmp->coefs.ac2 * b6) >> 11;
    x3 = x1 + x2;
    b3 = (((((long)bmp->coefs.ac1) * 4 + x3) << bmp->mode) + 2) >> 2;
    /* calculate B4 */
    x1 = (bmp->coefs.ac3 * b6) >> 13;
    x2 = (bmp->coefs.b1 * ((b6 * b6) >> 12)) >> 16;
    x3 = ((x1 + x2) + 2) >> 2;
    b4 = (bmp->coefs.ac4 * (unsigned long)(x3 + 32768)) >> 15;
    b7 = ((unsigned long)(up - b3) * (50000 >> bmp->mode));

    if (b7 < 0x80000000)
        p = (b7 << 1) / b4;
    else
        p = (b7/b4)<<1;

    x1 = (p >> 8) * (p >> 8);
    x1 = (x1 * 3038) >> 16;
    x2 = (-7357 * p) >> 16;
    p += (x1 + x2 + 3791) >> 4;
    *press = p;
    return BMP085_OK;
}

int bmp085_init(struct bmp085 *bmp, const char *dev) {
    int ret;
    bmp->fd = open(dev, O_RDWR);
    if (bmp->fd < 0)
        return BMP085_INVAL_DEV;

    ret = ioctl(bmp->fd, I2C_SLAVE, BMP085_ADDRESS);
    if (ret < 0)
        return BMP085_INVAL_DEV;

    ret = bmp085_read_coefs(bmp);
    if (ret != BMP085_OK)
        return ret;

    bmp->mode = BMP085_STD;
    return BMP085_OK;
}

float bmp085_press_to_sea(float press, float alt) {
    return press/pow((1 - (alt/44330.0)), 5.255);
}

float bmp085_press_to_altitude(float sea, float press) {
    return 44330.0 * (1 - pow(press/sea, 1.0/5.255));
}
